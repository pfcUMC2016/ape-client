app.controller('ConsultaSegmentoController', ['$scope', '$http', 'logger', 'listSegmentos', 
	function($scope, $http, logger, listSegmentos){
	window.scope = $scope;

	$scope.listSegmento = listSegmentos;



	$scope.deleteSegmento = function(segmento){
		$http.delete('segmento/delete/' + segmento.id).then(function(response){
			if(response.status == 200){
				logger.logSuccess('Segmento deletado com sucesso');
				$scope.listSegmento.splice($scope.listSegmento.indexOf(segmento),1);
			}
		})
	}
	
}]);