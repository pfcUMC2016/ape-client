app.controller('SegmentoController', ['$scope', '$http', 'segmento', 'logger', '$state',function($scope, $http, segmento, logger,$state){
	window.scope = $scope;
	$scope.segmento = segmento;



	
	$scope.salvarSegmento = function(){
		var params = {
		};
		$http.post("segmento/cadastro/new", $scope.segmento).then(
			function(response){
				if(response.status == 200){
					logger.logSuccess('Segmento Cadastrado com Sucesso');
					$state.go("segmento/consulta")
					
				}else{
					logger.logError("Erro ao cadastrar Segmento")
				}
			}	
		);
	}
	
}]);