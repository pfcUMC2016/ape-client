app.controller('LoginController', ['$scope','$http','$location', function($scope,$http, $location ){
	window.scope = $scope;
	$scope.pageComplete = true;
	$scope.usuario = {};
	

	var config = {
		headers:  {
		'Content-Type': 'application/x-www-form-urlencoded'
		}
    };

	$scope.efetuarLogin = function(){
		$http.post('login', $.param($scope.usuario),config)
			.success(
				function(data, status, headers, config){
					localStorage.setItem("tokenApe",headers('X-AUTH-TOKEN') );
					$location.path("/dashboard");
				}
			)
			.error(
				function(data, status, headers, config){
					console.log(data);
				}
			);
	}

}]);
