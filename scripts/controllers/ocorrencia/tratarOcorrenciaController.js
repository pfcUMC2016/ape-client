app.controller('ComentarioController', ['$scope', '$uibModalInstance', 'ocorrencia', 'logger', '$http', function ($scope, $uibModalInstance, ocorrencia, logger, $http) {

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.addComentario = function(){
        var historico = {
            'id': null,
            'ocorrencia': {
                'id': ocorrencia.id
            },    
            'acao':{
                'id': null,
                'comentario': $scope.comentario
            }
        }

        $http.post('ocorrencia/comentario', historico).then(function(response){
            if(response.status == 200){
                logger.logSuccess('Comentario adicionado na ocorrência Nº: ' + ocorrencia.id);
                $uibModalInstance.dismiss('cancel');
            }
        })
    }

}]);

app.controller('OcorrenciaStatusController', ['$scope', '$uibModalInstance', 'ocorrencia', 'logger', '$http', '$state', 'selectedStatus', function ($scope, $uibModalInstance, ocorrencia, logger, $http, $state, selectedStatus) {

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.status = selectedStatus;
  

    $scope.action = function(){
         var historico = {
            'id': null,
            'ocorrencia': {
                'id': ocorrencia.id
            },    
            'acao':{
                'id': null,
                'comentario': $scope.comentario
            },
            'status': {
                'id':$scope.status.id
            }
        }

        $http.post('ocorrencia/changeStatus', historico).then(function(response){
            if(response.status == 200){
                logger.logSuccess('Mudança de Status realizada na ocorrência Nº: ' + ocorrencia.id);
                $uibModalInstance.dismiss('cancel');
                $state.reload();
            }
        })
    }

}]);

app.controller('AdicionaServicoController', ['$scope', '$uibModalInstance', 'ocorrencia', 'logger', '$http','$state', function ($scope, $uibModalInstance, ocorrencia, logger, $http, $state) {

    $scope.servicos = [];


    $http.get('servico/listAllServicos').then(
        function(response){
            if(response.status == 200){
                $scope.servicos = response.data.resultado.listServicos;
                console.log($scope.servicos);
            }
        }
    )
    


    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.addServico = function(){
        var servico = {
            'id': null,
            'ocorrencia': {
                'id': ocorrencia.id
            },    
            'servico':{
                'id': $scope.servico
            }
        }

        $http.post('ocorrencia/servico', servico).then(function(response){
            if(response.status == 200){
                logger.logSuccess('Adicionado serviço na ocorrência Nº: ' + ocorrencia.id);
                $uibModalInstance.dismiss('cancel');
                $state.reload();
            }
        })
    }

}]);



app.controller('TratarOcorrenciaController', ['$scope', '$http', '$state', 'logger', '$uibModal', 'ocorrencia', 'OcorrenciaService','status', function ($scope, $http, $state, logger, $uibModal, ocorrencia, OcorrenciaService, status) {
    console.log('oc', ocorrencia);
    $scope.ocorrencia = ocorrencia;
    $scope.modal = {};
    $scope.ordemServico = {};
    $scope.listaStatus = status;
    $scope.statusAtual = ocorrencia.status.id; 
    $scope.sameUser = false;

    window.scope = $scope;

    if($scope.userInfo.username == $scope.ocorrencia.usuario.usuario){
        $scope.sameUser = true;
    }

    $scope.init = function(){
        $scope.ordemServico.total = 0;
        $scope.ordemServico.totalAprovado = 0;
        $scope.ordemServico.faltaAprovar = 0;
        for(var i = 0; i < $scope.ocorrencia.servicos.length; i++){
            var servico = $scope.ocorrencia.servicos[i].servico;
            $scope.ordemServico.total += servico.valor;
            if(scope.ocorrencia.servicos[i].aprovado == true){
              $scope.ordemServico.totalAprovado +=  servico.valor;
            } else {
                $scope.ordemServico.faltaAprovar += 1;
            }
        }
    }

    $scope.modal.comentario = {
        'open': function () {
            $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title-top',
                ariaDescribedBy: 'modal-body-top',
                templateUrl: 'views/ocorrencia/comentario.html',
                size: 'md',
                controller: 'ComentarioController',
                resolve: {
                    'ocorrencia': $scope.ocorrencia
                }
            });
        }
    }

    $scope.modal.servico = {
        'open': function () {
            $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title-top',
                ariaDescribedBy: 'modal-body-top',
                templateUrl: 'views/ocorrencia/servico.html',
                size: 'md',
                controller: 'AdicionaServicoController',
                resolve: {
                    'ocorrencia': $scope.ocorrencia
                }
            });
        }
    }




   

    $scope.aprovarService = function(id){
        $http.get('ocorrencia/servico/aprovar/' + id).then(function(response){
            if(response.status == 200){
                logger.logSuccess("Serviço Aprovado");
                $state.reload();
            }
        })

    }


    $scope.excludeService = function(id){
        $http.delete('ocorrencia/servico/' + id).then(function(response){
            if(response.status == 200){
                logger.logSuccess("Serviço deletado");
                $state.reload();   
            }
        })
         
    }

   
    $scope.modal.status = {
        'open': function (selectedStatus) {
             this.modal_f = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title-top',
                ariaDescribedBy: 'modal-body-top',
                templateUrl: 'views/ocorrencia/finalizacao.html',
                size: 'md',
                controller: 'OcorrenciaStatusController',
                resolve: {
                    'ocorrencia': $scope.ocorrencia,
                    'selectedStatus': selectedStatus
                }
            });
        },
        'close': function(){
            console.log("fechou", this.modal_f);
        }
    }

    $scope.reabrir = function(){
        var status =  {
            'id': 5,
            'nmeStatus': 'REABERTO'
        }
        $scope.modal.status.open(status);
    }

    $scope.atribuirForMe = function(){
        $http.get('ocorrencia/me' + id).then(function(response){
            if(response.status == 200){
                $state.reload();
            }
        })
    }

    

    function mountEvent (event){
        var icon = '';
        var acao = '';
        var noText = true;
        if(event.acao.tipoAcao == 'CRIACAO'){
            acao = 'criou a ocorrência';
            icon = 'fa fa-circle bg-blue';
            noText = true;
        }
        if(event.acao.tipoAcao == 'COMENTARIO'){
            acao = 'comentou na ocorrência';
            icon = 'fa fa-comments bg-yellow';
            noText = false;
        }
        if(event.acao.tipoAcao == "MUDANCA_DE_STATUS"){
            acao = 'mudou o status da ocorrência para ' +  event.status.nmeStatus;
            icon = 'fa fa-code-fork';
            if(event.acao.comentario != "" && event.acao.comentario != null)
                noText = false;   
        }

        event.date = moment(event.acao.dtaAcao, 'DD-MM-YYYY HH:mm:ss').format("DD.MM.YYYY");
        event.hour = moment(event.acao.dtaAcao, 'DD-MM-YYYY HH:mm:ss').format("HH:mm:ss");
        event.ago = moment(event.acao.dtaAcao, 'DD-MM-YYYY HH:mm:ss').fromNow(); 
        event.icon = icon;
        event.action = acao;
        event.noText = noText; 
        return event;
    }
    var handleSuccess = function(data, status) {
        $scope.line = data.resultado;
        console.log($scope.line);
        $scope.timeline = [];
        for(var i=0; i < $scope.line.actions.length; i++){
            $scope.line.actions[i] = mountEvent($scope.line.actions[i]);
            if(i == 0){
                $scope.timeline.push([ $scope.line.actions[i] ]);
            } else {
                if( $scope.line.actions[i].date ==  $scope.line.actions[i-1].date){
                    $scope.timeline[$scope.timeline.length -1].push($scope.line.actions[i])
                } else{
                    $scope.timeline[$scope.timeline.length] = [$scope.line.actions[i]];
                }
            }
        }
        console.log("TIME", $scope.timeline);
    };

    $scope.atualizarTimeLine = function(){
        console.log('atualizando');
        OcorrenciaService.getActions($scope.ocorrencia.id).success(handleSuccess);
    }


}]);



