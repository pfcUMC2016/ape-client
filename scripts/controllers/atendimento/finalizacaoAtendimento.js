app.controller('FinalizacaoAtendimentoController', ['$scope', '$http', 'logger', '$state', function($scope, $http, logger, $state) {
	window.scope = $scope;
	$scope.pageComplete = true;
	$scope.atendimento = $scope.$parent.atendimento;
	

	$scope.finalizarAtendimento = function(){
		$scope.atend = angular.copy($scope.atendimento);
		var params = {
			'id': $scope.atend.id,
			'descricao': $scope.atend.descricao
		}
		$scope.atend.usuario = {};
		$http.post('atendimento/finalizaAtendimento', params).success(
			function(data){
				$scope.atendimento = null;
				$state.go("dashboard");
				
			})
		
	}

}]);