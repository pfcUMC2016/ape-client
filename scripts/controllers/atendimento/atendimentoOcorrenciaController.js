app.controller('AtendimentoOcorrenciaController', ['$scope','$http','$state', 'treeview', 'logger', 'prioridades', 'status', 'historico','area', 'veiculos', function($scope, $http, $state, treeview, logger, prioridades, status, historico, area, veiculos){

window.scope = $scope;

$scope.area = area;
$scope.treeview = treeview;
$scope.selectedType = null;
$scope.veiculos = veiculos;
$scope.prioridades = prioridades;
$scope.usuarios = {};
$scope.status = status;
$scope.atendimento = $scope.$parent.atendimento;
$scope.historico = historico;
$scope.ocorrencia = {};
$scope.outroUsuario = false;
$scope.ocorrencia.usuarioSelecionado = "0"; // DISTRIBUICAO DO SISTEMA


$scope.selecionaUsuario = function(){
	if($scope.outroUsuario == false){
		$scope.ocorrencia.usuarioSelecionado = "0";
	}
}



$scope.selectedBranch = function(branch){
 	$scope.ocorrencia.tipoOcorrencia = branch;
 	if(branch.children.length == 0){
 		$scope.ocValid = true;
 		$scope.ocorrencia.area = String(branch.area.id);
	 	$http.get('usuario/area/' + branch.area.id ).then(
			function(response){
				$scope.usuarios =  response.data.resultado.usuarios;
				console.log($scope.usuarios);
			},
			function(errResponse){
				return $q.reject(errResponse);
			}
		);
 	}else {
 		$scope.ocValid = false;
 	}
}

$scope.gerarOcorrencia = function(){
  if($scope.ocorrencia.tipoOcorrencia == null || $scope.ocorrencia.tipoOcorrencia.children.length > 0){
    logger.logWarning("Selecione um tipo de ocorrencia válida.")
  } else {

	  var param = 
	  {
			"ocorrencia":{
				"id" : null,
				"dscOcorrencia": $scope.ocorrencia.dscOcorrencia,
				"status": {
					"id": 1
				},
				"veiculo":{
					'id': $scope.ocorrencia.veiculo
				},
				"prioridade":{
					"id": $scope.ocorrencia.prioridade
				},
				"tipo":{
					"id": $scope.ocorrencia.tipoOcorrencia.id
				},
				"atendimento" : {
					"id" : $scope.atendimento.id
				},
				"cliente": $scope.atendimento.cliente
			},
			"usuario":{
					"id": $scope.ocorrencia.usuarioSelecionado
			},
			"area":{
				'id': $scope.ocorrencia.area
			}
		  }


    	$http.post('ocorrencia/criar', param).then(function(response){
    		if(response.status == 200){
    			logger.logSuccess("Ocorrencia Salva");
    			$state.reload();
    		}
    	})
  }
}

}]);


app.controller('AssociarVeiculoController', ['$scope', '$uibModalInstance', 'ocorrencia', 'logger', '$http', function ($scope, $uibModalInstance, ocorrencia, logger, $http) {

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.addComentario = function(){
        var historico = {
            'id': null,
            'ocorrencia': {
                'id': ocorrencia.id
            },    
            'acao':{
                'id': null,
                'comentario': $scope.comentario
            }
        }

        $http.post('ocorrencia/comentario', historico).then(function(response){
            if(response.status == 200){
                logger.logSuccess('Comentario adicionado na ocorrência Nº: ' + ocorrencia.id);
                $uibModalInstance.dismiss('cancel');
            }
        })
    }

}]);