app.controller('AssociarClienteController', ['$scope','$http','$state', 'ClienteService', 'logger', function($scope, $http, $state, ClienteService, logger){
	$scope.listaClientes = [];


	if($scope.$parent.atendimento.cliente != undefined){
		$scope.cliente = $scope.$parent.atendimento.cliente;
	}

	$scope.pesquisar = function(){
		console.log("chamou")
		 $http.get('clientes/pesquisa/'+ $scope.nomeCliente)
    		.then(function(response) {
    			if(response.data.resultado.clientes.length > 0) {
    				$scope.listaClientes = response.data.resultado.clientes;	
    			}else{
    				logger.logWarning("Nenhum cliente encontrado.")
    			}
        		
    		});
	}

	$scope.editarDados = function(id){
		$state.go("cliente/cadastro/:idCliente", {idCliente:id});
	};


$scope.associaCliente = function(id){
	//$scope.$emit('associaCliente', id);
	$scope.$parent.associarCliente(id);
//	$state.reload();
	//$scope.cliente = $scope.$parent.atendimento.cliente;
};

}]);