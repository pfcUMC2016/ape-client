app.controller('CadastroVeiculoController', ['$scope', '$http', 'responseCtrl', 'logger','listarCores','todosClientes','veiculo','$state' ,
              function($scope, $http, responseCtrl, logger, listarCores, todosClientes, veiculo, $state){
	window.scope = $scope;
	$scope.veiculo = veiculo.veiculoCliente;
	$scope.cor = {};
	$scope.veiculoCliente= {};
	$scope.listMarcas = responseCtrl.data.listMarcas;
	//$scope.listClientes = todosClientes.listClientes;
	$scope.listCliente = todosClientes.resultado.listClientes;
	$scope.listCor = listarCores.listCor;
	
	
	$scope.cadastrarVeiculoCliente = function(){
		var params = {
		};
		$http.post( "veiculo/cadastro/new" , $scope.veiculo).then(
			function(response){
				if(response.status == 200){
					logger.logSuccess('Veiculo salvo.');	
					$state.go('veiculo/consultaVeiculo');		
				} else {
					logger.logError("Erro ao cadastrar Cliente")
				}
			}
			);
	}
	
	$scope.changeMarca = function(){
		$scope.listModelos = [];
		if($scope.veiculo.veiculoMarca != "" && typeof $scope.veiculo.veiculoMarca != "undefined" ){
			$http.get('veiculo/listModelos?idMarca=' + $scope.veiculo.veiculoMarca.id).success(
					function(data){
					$scope.listModelos = data.listModelo;
				})
		}
	}
	
	$scope.changeMarca();
}]);