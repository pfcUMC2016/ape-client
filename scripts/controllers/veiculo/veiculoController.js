app.controller('veiculoController', ['$scope', '$http', 'responseCtrl', 'logger', function($scope, $http, responseCtrl, logger ){
	window.scope = $scope;

	$scope.veiculoCliente= {};
	$scope.listarVeiculoCliente = responseCtrl.listVeiculoCliente;

	
	
	$scope.deleteVeiculoCliente = function(veiculoCliente){
		$http.delete('veiculo/delete/' + veiculoCliente).then(function(response){
			if(response.status == 200){
				logger.logSuccess('Veiculo deletado com sucesso');
				$scope.listarVeiculoCliente.splice($scope.listarVeiculoCliente.indexOf(veiculoCliente),1);
			}
		})
	}
	
}]);