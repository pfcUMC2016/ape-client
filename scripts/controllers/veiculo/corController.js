app.controller('corController', ['$scope', '$http', 'responseCtrl', 'logger', function($scope, $http, responseCtrl, logger){
	window.scope = $scope;

	
	$scope.veiculoCor= {};
	$scope.listarCor = responseCtrl.listCor;

	
	
	$scope.deleteCor = function(veiculoCor){
		$http.delete('veiculo/deleteCor/' + veiculoCor).then(function(response){
			if(response.status == 200){
				logger.logSuccess('Cor deletado com sucesso');
				$scope.listarCor.splice($scope.listarCor.indexOf(veiculoCor),1);
			}
			}	
		);
	}
	
	
}]);