app.controller('consultaModeloController', ['$scope', '$http', 'responseCtrl', 'logger', function($scope, $http, responseCtrl, logger){
	window.scope = $scope;

	
	$scope.veiculoModelo= {};
	$scope.listTodosModelos = responseCtrl.data.listTodosModelos;


	$scope.deleteModelo = function(veiculoModelo){
		$http.delete('veiculo/deleteModelo/' + veiculoModelo).then(function(response){
			if(response.status == 200){
				logger.logSuccess('Modelo deletado com sucesso');
				$scope.listTodosModelos.splice($scope.listTodosModelos.indexOf(veiculoModelo),1);
			}
			}	
		);
	}
	
	
	
}]);