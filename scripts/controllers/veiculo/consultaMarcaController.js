app.controller('consultaMarcaController', ['$scope', '$http', 'responseCtrl', 'logger',  function($scope, $http, responseCtrl, logger){
	window.scope = $scope;

	
	$scope.listMarcas = responseCtrl.data.listMarcas;

$scope.deletarMarca = function(veiculoMarca){
		$http.delete('veiculo/deleteMarca/' + veiculoMarca).then(function(response){
			if(response.status == 200){
				logger.logSuccess('Marca deletada com sucesso');
				$scope.listMarcas.splice($scope.listMarcas.indexOf(veiculoMarca),1);
			}
			}	
		);
	}
	
	
	
}]);