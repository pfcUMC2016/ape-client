app.controller('CadastroMarcaController', ['$scope', '$http', 'marca', 'logger', '$state', function($scope, $http, marca, logger,$state){
	window.scope = $scope;
	$scope.marca = marca;

	
	$scope.salvarVeiculoMarca = function(){
		var params = {
		};
		$http.post( "veiculo/cadastroVeiculoMarca/new" , $scope.marca).then(
			function(response){
				if(response.status == 200){
					logger.logSuccess('Marca salva.');	
					$state.go('veiculo/consultaMarca');	
				} else {
					logger.logError("Erro ao cadastrar Marca")
				}
			}
			);
	}
	
}]);