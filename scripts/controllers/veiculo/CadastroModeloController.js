app.controller('CadastroModeloController', ['$scope', '$http', 'responseCtrl', 'logger','modelo', '$state', function($scope, $http, responseCtrl, logger,modelo ,$state){
	window.scope = $scope;
	$scope.modelo = modelo;
	$scope.listMarcas = responseCtrl.data.listMarcas;

	
	$scope.cadastrarModelo = function(){
		$http.post("veiculo/cadastroVeiculoModelo/new", $scope.modelo).then(
			function(response){
				if(response.status == 200){
					logger.logSuccess('Modelo cadastrado');
					$state.go('veiculo/consultaModelo');	
				}else{
					logger.logError("Erro ao cadastrar Modelo")
				}
			}	
		);
	}
	
}]);