app.controller('CadastroCorController', ['$scope', '$http', 'logger','$state', 'cor', function($scope, $http, logger, $state, cor){
	window.scope = $scope;
	
	console.log(cor);
	$scope.cor = cor;


	$scope.cadastrarCor = function(){
		$http.post("veiculo/cadastroCor/new", $scope.cor).then(
			function(response){
				if(response.status == 200){
					logger.logSuccess('Cor cadastrada');
					$state.go('veiculo/consultaCor');
				}else{
					logger.logError("Erro ao cadastrar Cor")
				}
			}	
		);
	}
	
}]);