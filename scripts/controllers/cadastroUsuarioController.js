app.controller('CadastroUsuarioController', ['$scope', '$http', 'listPermissoes','listUsuarios','logger', '$state', 'area',
              function($scope, $http, listPermissoes, listUsuarios, logger, $state, area){
	window.scope = $scope;
	$scope.listaPermissoes = listPermissoes.data.listaPermissoes;
	$scope.permissoes = []
	$scope.listaUsuario = listUsuarios;
	console.log(listUsuarios);

	$scope.area = area;
	
	$scope.newUser = false;


	$scope.novoUsuario = function(){
		$scope.newUser = true;	
		$scope.usuario = {}
	}

	
	$scope.salvarUsuario = function(){
		$scope.usuario.area = {
			'id': $scope.usuario.area
		};
		$http.post('usuario/novoUsuario', $scope.usuario)
		.success(function(data){
			logger.logSuccess('Usuário salvo com Sucesso.');
			$scope.listaUsuario = data.listaUsuarios;
			$state.reload();
		})
	}
	
	$scope.editUser= function(user){
		console.log(user);
		$scope.newUser = true;
		$scope.usuario = user;
		$scope.usuario.ocorrencias = null;
		$scope.usuario.area = String($scope.usuario.area.id);
		$scope.selectedPermissoes = [];
//		for (var i = 0; i < user.perfil.length; i++) {
//			$scope.selectedPermissoes[i] = {
//					'id'     : user.perfil[i].id,
//					'name'   : user.perfil[i].permissao,
//					'ticked' : true
//			}
//		}
	}
                                                                                     
}]);