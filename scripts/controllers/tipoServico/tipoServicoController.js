app.controller('tipoServicoController', ['$scope', '$http', 'logger', '$state', 'listOne',function($scope, $http, logger,$state,listOne){
	window.scope = $scope;
	$scope.tipoServico = listOne;



	
	$scope.salvartipoServico = function(){
		var params = {
		};
		$http.post("tipoServico/cadastro/new", $scope.tipoServico).then(
			function(response){
				if(response.status == 200){
					logger.logSuccess('Unidade de Medida Cadastrada com Sucesso');
					$state.go("tipoServico/consulta")
				}else{
					logger.logError("Erro ao cadastrar Unidade de Medida")
				}
			}	
		);
	}
	
}]);