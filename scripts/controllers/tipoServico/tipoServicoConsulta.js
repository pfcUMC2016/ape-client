app.controller('tipoServicoConsulta', ['$scope', '$http', 'logger', 'listarAllTipos', function($scope, $http, logger, listarAllTipos){
	window.scope = $scope;

	$scope.listAllTipo = listarAllTipos

	$scope.deletarTipoServico = function(tipoServico){
		$http.delete('tipoServico/delete/' + tipoServico.id).then(function(response){
			if(response.status == 200){
				logger.logSuccess('Tipo deletado com sucesso');
				$scope.listAllTipos.splice($scope.listAllTipos.indexOf(listAllTipos),1);
			}
		})
	}
	
}]);