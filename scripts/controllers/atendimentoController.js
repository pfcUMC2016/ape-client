app.controller('AtendimentoController', ['$scope','$http','$state', 'logger', 'atendimento', function($scope, $http, $state, logger, atendimento){
	window.scope = $scope;
	var vm = this;
	$scope.pageComplete = true;
	$scope.atendimento = atendimento;
	if($state.params.idAtendimento == ""){
		$state.go("atendimento", {'idAtendimento':  $scope.atendimento.id  });
	}
	
	

function update(){
		setInterval(function(){
			$scope.$apply(function () {
				var now = moment(new Date()).format("DD-MM-YYYY HH:mm:ss");
				var then = new Date();
				$scope.tempoAtendimento = moment($scope.atendimento.dtaInicio, 'DD-MM-YYYY HH:mm:ss').fromNow(); 
				// moment.utc(moment(now,"DD-MM-YYYY HH:mm:ss").diff(moment(then,"DD-MM-YYYY HH:mm:ss"))).format("HH:mm:ss");
			})
		}, 1000);
	}

	update();
	
	
	$scope.iniciarAtendimento = function(){
		$http.get('atendimento/returnAtendimento').then( function(response){
		if(response.status == 200){
			$scope.atendimento = response.data;
		}
		})
		update();
	}

	$scope.atualizaBarra = function(){
		$http.get('atendimento/returnAtendimento').then( function(response){
			if(response.status == 200){
				$scope.atendimento = response.data.resultado.atendimento;
			}
		})
	}

	$scope.associarCliente = function(id){
		var params = {
			'id' : $scope.atendimento.id,
			'cliente' :{
				'id' : id
			}
		}
		$http.post("atendimento/associaCliente", params).then(function(response){
			if(response.status == 200){
				logger.logSuccess("Cliente associado com sucesso !");
				console.log("parent", $scope.$parent);
				$state.reload();
			}
		})
	}
	
// $scope.$on('associaCliente', function(event, args) {
	
// });

	
//atualizaBarra();
	

}]);