app.controller('ClienteController', ['$scope', '$http', 'clientes', 'logger', function($scope, $http, clientes, logger) {
	window.scope = $scope;
	
	$scope.listCliente = clientes.resultado.listClientes;
	
	
	$scope.deleteCliente = function(cliente){
		$http.delete('clientes/delete/' + cliente.id).then(function(response){
			if(response.status == 200){
				logger.logSuccess('Usuário deletado com sucesso');
				$scope.listCliente.splice($scope.listCliente.indexOf(cliente),1);
			}
		})
	}
}]);