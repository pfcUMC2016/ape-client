	
app.controller('CadastroClienteController', ['$scope', '$http', 'logger', '$state','cliente',
              function($scope, $http, logger, $state, cliente) {
	window.scope = $scope;
	$scope.cliente = cliente;
	
	$scope.consultaCep = function(){
		//https://viacep.com.br/ws/01001000/json/
		//https://viacep.com.br/ws/01001000/json/?callback=callback_name
		$http.jsonp('api://https://viacep.com.br/ws/'+  $scope.cliente.endereco.cep + '/json/?callback=JSON_CALLBACK').then(
			function(response){
				if(response.status == 200){
					if(response.data.hasOwnProperty("erro")){
						logger.logWarning("Cep não encontrado");
					}
					$scope.cliente.endereco.logradouro = response.data.logradouro;
					$scope.cliente.endereco.bairro = response.data.bairro;
					$scope.cliente.endereco.cidade = response.data.localidade;
					$scope.cliente.endereco.uf = response.data.uf;
					$scope.cliente.endereco.complemento = response.data.complemento;
				}
		})
	}
	

	
	$scope.cadastrarCliente = function(){
		var params = {
		};
		$http.post( "clientes/cadastro/new" , $scope.cliente).then(
			function(response){
				if(response.status == 200){
					logger.logSuccess('Cliente cadastrado com Sucesso');
					$state.go("cliente")
				} else {
					logger.logError("Erro ao cadastrar Cliente")
				}
			}
			);
	}

}]);