app.controller('CadastroServicoController', ['$scope', '$http', 'logger', 'servico', '$state','listarAllTipos','listarAllFornecedor',
	function($scope, $http, logger,servico,$state, listarAllTipos,listarAllFornecedor){
	window.scope = $scope;
	

	$scope.servico = servico;


	$scope.listAllTipo = listarAllTipos;
	$scope.listFornecedor = listarAllFornecedor;


	
	$scope.salvarServico = function(){
		$http.post("servico/cadastro/new", $scope.servico).then(
			function(response){
				if(response.status == 200){
					logger.logSuccess('Serviço Cadastro com Sucesso');
					$state.go("servico/consulta")
				}else{
					logger.logError("Erro ao cadastrar Serviço")
				}
			}	
		);
	}
	
}]);