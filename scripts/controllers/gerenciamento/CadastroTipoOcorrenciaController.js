app.controller('CadastroTipoOcorrenciaController', ['$scope', '$state' , 'treeview', '$uibModal',  function($scope, $state, treeview, $uibModal){
	var tree;
	window.scope = $scope;
	console.log(treeview);
	$scope.my_tree = tree = {};
	$scope.treeview = treeview;
	$scope.selected = null;
	$scope.modal = {};

	console.log(tree);

	$scope.selectedBranch = function(branch){
		$scope.selected = branch;
	}


	 $scope.modal.item = {
        'open': function (branch,edit) {
            $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title-top',
                ariaDescribedBy: 'modal-body-top',
                templateUrl: 'views/gerenciamento/popupTipo.html',
                size: 'md',
                controller: 'CadastroItemController',
                resolve: {
                    'itemPai': branch,
                    'edit': edit
                }
            });
        }
	}

	 return $scope.try_adding_a_branch = function() {
      var b;
      b = tree.get_selected_branch();
      console.log("essa é a branch selecionada", b);
      return tree.add_branch(b, {
        label: 'New Branch',
        data: {
          something: 42,
          "else": 43
        }
      }, function(b){
      	console.log(b);
      });
    };


}]);

app.controller('CadastroItemController', ['$scope', '$uibModalInstance', 'itemPai', 'logger', '$http', 'UsuarioService', '$state', 'edit', function ($scope, $uibModalInstance, itemPai, logger, $http, UsuarioService, $state, edit) {

	// $scope.area = UsuarioService.area().then(function(response){
	// 	return response;
	// });

	UsuarioService.area().then(function(result){
		$scope.area = result;
	})
	console.log(itemPai);
	$scope.edit = edit;
	window.scope = $scope;
	if(itemPai != null){
		$scope.item = {
		'id': null,
		'nmeTipo': null,
		'sla': null,
		'parent':{
			'id': itemPai.id
		},
		'area':{
			'id':null
		}
	};	
	} else {
		$scope.item = {
		'id': null,
		'nmeTipo': null,
		'sla': null,
		'area':{
			'id':null
		}
	};
	}

	if(edit == true){
		$scope.item = itemPai;
		$scope.item.nmeTipo = itemPai.label;
		$scope.item.area = {
			'id': String(itemPai.area.id)
		}
	}
	
	
	$scope.itemPai = itemPai;
	console.log($scope.itemPai);
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.salvar = function(){
        // var historico = {
        //     'id': null,
        //     'ocorrencia': {
        //         'id': ocorrencia.id
        //     },    
        //     'acao':{
        //         'id': null,
        //         'comentario': $scope.comentario
        //     }
        // }
        $http.post('ocorrencia/arvore/item/new', $scope.item).then(function(response){
            if(response.status == 200){
                logger.logSuccess('item Cadastrado com sucesso');
                $uibModalInstance.dismiss('cancel');
                $state.reload();
            }
        })
    }

}]);