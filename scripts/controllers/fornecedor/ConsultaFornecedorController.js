app.controller('ConsultaFornecedorController', ['$scope', '$http', 'logger', 'listaFornecedor', function($scope, $http, logger, listaFornecedor){
	window.scope = $scope;

	$scope.listFornecedor = listaFornecedor;

	$scope.deletarFornecedor = function(fornecedor){
		$http.delete('fornecedor/delete/' + fornecedor.id).then(function(response){
			if(response.status == 200){
				logger.logSuccess('Fornecedor Deletado com sucesso');
				$scope.listaFornecedor.splice($scope.listaFornecedor.indexOf(fornecedor),1);
			}
		})
	}
	
}]);