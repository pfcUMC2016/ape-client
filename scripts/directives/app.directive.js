app.directive('customMask', ['safeApply', function(safeApply) {
	return {
		restrict: 'A',
		require: 'ngModel',
		replace: true,
		link : function (scope, element, attrs, controller) {
			var model = attrs.customMask;
			var reverse = attrs.customMaskReverse;
			
			var options = {
				reverse : reverse,
				onChange : function(value) {
					controller.$setViewValue(value);
					safeApply(scope);
				}
			}
			$(element).mask(model, options);
		}
	}
}]);

app.directive("menuTabs", function($timeout){
  return {
    link:function(scope, element, attrs){
		$timeout(function(){
        	var elementos = $("li.treeview");
			for(var i=0; i < elementos.length; i++){
				var procurando = $(elementos[i]).find(".active");
				if(procurando.length > 0){
					$(elementos[i]).addClass("active");
				}
			}	
		}, 1000); 
    }
  }
});

 app.directive('icheck', ['$timeout', '$parse', function($timeout, $parse) {
    return {
      restrict: 'A',
      require: '?ngModel',
      link: function(scope, element, attr, ngModel) {
        $timeout(function() {
          var value = attr.value;
        
          function update(checked) {
            if(attr.type==='radio') { 
              ngModel.$setViewValue(value);
            } else {
              ngModel.$setViewValue(checked);
            }
          }
          
          $(element).iCheck({
            checkboxClass: attr.checkboxClass || 'icheckbox_square-green',
            radioClass: attr.radioClass || 'iradio_square-green'
          }).on('ifChanged', function(e) {
            scope.$apply(function() {
              update(e.target.checked);
            });
          });

          scope.$watch(attr.ngChecked, function(checked) {
            if(typeof checked === 'undefined') checked = !!ngModel.$viewValue;
            update(checked)
          }, true);

          scope.$watch(attr.ngModel, function(model) {
            $(element).iCheck('update');
          }, true);
          
        })
      }
    }
  }]);


app.directive('ngConfirmClick', [
  function(){
    return {
      priority: -1,
      restrict: 'A',
      link: function(scope, element, attrs){
        element.bind('click', function(e){
          var message = attrs.ngConfirmClick;
          if(message && !confirm(message)){
            e.stopImmediatePropagation();
            e.preventDefault();
          }
        });
      }
    }
  }
]);

app.directive('errSrc', function() {
  return {
    link: function(scope, element, attrs) {
      element.bind('error', function() {
        if (attrs.src != attrs.errSrc) {
          attrs.$set('src', attrs.errSrc);
        }
      });
    }
  }
});