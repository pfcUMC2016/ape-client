app.factory('logger', [
	function() {
		var logIt;

		toastr.options = {
			"closeButton": true,
			/*"preventDuplicates": true,*/
			"positionClass": "toast-bottom-right",
			"timeOut": "5000"
		};




		logIt = function(message, type) {
			return toastr[type](message);
		};

		return {
			log: function(message) {
				logIt(message, 'info');
			},

			logWarning: function(message) {
				logIt(message, 'warning');
			},

			logSuccess: function(message) {
				logIt(message, 'success');
			},

			logError: function(message) {
				logIt(message, 'error');
			},

			clearAll: function() {
				logIt(null, 'remove');
			}
		};
	}
	]);




app.factory('safeApply', [function($rootScope) {
    return function($scope, fn) {
        var phase = $scope.$root.$$phase;
        if(phase == '$apply' || phase == '$digest') {
            if (fn) {
                $scope.$eval(fn);
            }
        } else {
            if (fn) {
                $scope.$apply(fn);
            } else {
                $scope.$apply();
            }
        }
    }
}]);