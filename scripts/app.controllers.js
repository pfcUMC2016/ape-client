app.controller('AssociarClienteController', ['$scope','$http','$state', 'ClienteService', 'logger', function($scope, $http, $state, ClienteService, logger){
	$scope.listaClientes = [];


	if($scope.$parent.atendimento.cliente != undefined){
		$scope.cliente = $scope.$parent.atendimento.cliente;
	}

	$scope.pesquisar = function(){
		console.log("chamou")
		 $http.get('clientes/pesquisa/'+ $scope.nomeCliente)
    		.then(function(response) {
    			if(response.data.resultado.clientes.length > 0) {
    				$scope.listaClientes = response.data.resultado.clientes;	
    			}else{
    				logger.logWarning("Nenhum cliente encontrado.")
    			}
        		
    		});
	}

	$scope.editarDados = function(id){
		$state.go("cliente/cadastro/:idCliente", {idCliente:id});
	};


$scope.associaCliente = function(id){
	//$scope.$emit('associaCliente', id);
	$scope.$parent.associarCliente(id);
//	$state.reload();
	//$scope.cliente = $scope.$parent.atendimento.cliente;
};

}]);
app.controller('AtendimentoOcorrenciaController', ['$scope','$http','$state', 'treeview', 'logger', 'prioridades', 'status', 'historico','area', 'veiculos', function($scope, $http, $state, treeview, logger, prioridades, status, historico, area, veiculos){

window.scope = $scope;

$scope.area = area;
$scope.treeview = treeview;
$scope.selectedType = null;
$scope.veiculos = veiculos;
$scope.prioridades = prioridades;
$scope.usuarios = {};
$scope.status = status;
$scope.atendimento = $scope.$parent.atendimento;
$scope.historico = historico;
$scope.ocorrencia = {};
$scope.outroUsuario = false;
$scope.ocorrencia.usuarioSelecionado = "0"; // DISTRIBUICAO DO SISTEMA


$scope.selecionaUsuario = function(){
	if($scope.outroUsuario == false){
		$scope.ocorrencia.usuarioSelecionado = "0";
	}
}



$scope.selectedBranch = function(branch){
 	$scope.ocorrencia.tipoOcorrencia = branch;
 	if(branch.children.length == 0){
 		$scope.ocValid = true;
 		$scope.ocorrencia.area = String(branch.area.id);
	 	$http.get('usuario/area/' + branch.area.id ).then(
			function(response){
				$scope.usuarios =  response.data.resultado.usuarios;
				console.log($scope.usuarios);
			},
			function(errResponse){
				return $q.reject(errResponse);
			}
		);
 	}else {
 		$scope.ocValid = false;
 	}
}

$scope.gerarOcorrencia = function(){
  if($scope.ocorrencia.tipoOcorrencia == null || $scope.ocorrencia.tipoOcorrencia.children.length > 0){
    logger.logWarning("Selecione um tipo de ocorrencia válida.")
  } else {

	  var param = 
	  {
			"ocorrencia":{
				"id" : null,
				"dscOcorrencia": $scope.ocorrencia.dscOcorrencia,
				"status": {
					"id": 1
				},
				"veiculo":{
					'id': $scope.ocorrencia.veiculo
				},
				"prioridade":{
					"id": $scope.ocorrencia.prioridade
				},
				"tipo":{
					"id": $scope.ocorrencia.tipoOcorrencia.id
				},
				"atendimento" : {
					"id" : $scope.atendimento.id
				},
				"cliente": $scope.atendimento.cliente
			},
			"usuario":{
					"id": $scope.ocorrencia.usuarioSelecionado
			},
			"area":{
				'id': $scope.ocorrencia.area
			}
		  }


    	$http.post('ocorrencia/criar', param).then(function(response){
    		if(response.status == 200){
    			logger.logSuccess("Ocorrencia Salva");
    			$state.reload();
    		}
    	})
  }
}

}]);


app.controller('AssociarVeiculoController', ['$scope', '$uibModalInstance', 'ocorrencia', 'logger', '$http', function ($scope, $uibModalInstance, ocorrencia, logger, $http) {

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.addComentario = function(){
        var historico = {
            'id': null,
            'ocorrencia': {
                'id': ocorrencia.id
            },    
            'acao':{
                'id': null,
                'comentario': $scope.comentario
            }
        }

        $http.post('ocorrencia/comentario', historico).then(function(response){
            if(response.status == 200){
                logger.logSuccess('Comentario adicionado na ocorrência Nº: ' + ocorrencia.id);
                $uibModalInstance.dismiss('cancel');
            }
        })
    }

}]);
app.controller('FinalizacaoAtendimentoController', ['$scope', '$http', 'logger', '$state', function($scope, $http, logger, $state) {
	window.scope = $scope;
	$scope.pageComplete = true;
	$scope.atendimento = $scope.$parent.atendimento;
	

	$scope.finalizarAtendimento = function(){
		$scope.atend = angular.copy($scope.atendimento);
		var params = {
			'id': $scope.atend.id,
			'descricao': $scope.atend.descricao
		}
		$scope.atend.usuario = {};
		$http.post('atendimento/finalizaAtendimento', params).success(
			function(data){
				$scope.atendimento = null;
				$state.go("dashboard");
				
			})
		
	}

}]);
app.controller('AtendimentoController', ['$scope','$http','$state', 'logger', 'atendimento', function($scope, $http, $state, logger, atendimento){
	window.scope = $scope;
	var vm = this;
	$scope.pageComplete = true;
	$scope.atendimento = atendimento;
	if($state.params.idAtendimento == ""){
		$state.go("atendimento", {'idAtendimento':  $scope.atendimento.id  });
	}
	
	

function update(){
		setInterval(function(){
			$scope.$apply(function () {
				var now = moment(new Date()).format("DD-MM-YYYY HH:mm:ss");
				var then = new Date();
				$scope.tempoAtendimento = moment($scope.atendimento.dtaInicio, 'DD-MM-YYYY HH:mm:ss').fromNow(); 
				// moment.utc(moment(now,"DD-MM-YYYY HH:mm:ss").diff(moment(then,"DD-MM-YYYY HH:mm:ss"))).format("HH:mm:ss");
			})
		}, 1000);
	}

	update();
	
	
	$scope.iniciarAtendimento = function(){
		$http.get('atendimento/returnAtendimento').then( function(response){
		if(response.status == 200){
			$scope.atendimento = response.data;
		}
		})
		update();
	}

	$scope.atualizaBarra = function(){
		$http.get('atendimento/returnAtendimento').then( function(response){
			if(response.status == 200){
				$scope.atendimento = response.data.resultado.atendimento;
			}
		})
	}

	$scope.associarCliente = function(id){
		var params = {
			'id' : $scope.atendimento.id,
			'cliente' :{
				'id' : id
			}
		}
		$http.post("atendimento/associaCliente", params).then(function(response){
			if(response.status == 200){
				logger.logSuccess("Cliente associado com sucesso !");
				console.log("parent", $scope.$parent);
				$state.reload();
			}
		})
	}
	
// $scope.$on('associaCliente', function(event, args) {
	
// });

	
//atualizaBarra();
	

}]);
app.controller('CadastroUsuarioController', ['$scope', '$http', 'listPermissoes','listUsuarios','logger', '$state', 'area',
              function($scope, $http, listPermissoes, listUsuarios, logger, $state, area){
	window.scope = $scope;
	$scope.listaPermissoes = listPermissoes.data.listaPermissoes;
	$scope.permissoes = []
	$scope.listaUsuario = listUsuarios;
	console.log(listUsuarios);

	$scope.area = area;
	
	$scope.newUser = false;


	$scope.novoUsuario = function(){
		$scope.newUser = true;	
		$scope.usuario = {}
	}

	
	$scope.salvarUsuario = function(){
		$scope.usuario.area = {
			'id': $scope.usuario.area
		};
		$http.post('usuario/novoUsuario', $scope.usuario)
		.success(function(data){
			logger.logSuccess('Usuário salvo com Sucesso.');
			$scope.listaUsuario = data.listaUsuarios;
			$state.reload();
		})
	}
	
	$scope.editUser= function(user){
		console.log(user);
		$scope.newUser = true;
		$scope.usuario = user;
		$scope.usuario.ocorrencias = null;
		$scope.usuario.area = String($scope.usuario.area.id);
		$scope.selectedPermissoes = [];
//		for (var i = 0; i < user.perfil.length; i++) {
//			$scope.selectedPermissoes[i] = {
//					'id'     : user.perfil[i].id,
//					'name'   : user.perfil[i].permissao,
//					'ticked' : true
//			}
//		}
	}
                                                                                     
}]);
	
app.controller('CadastroClienteController', ['$scope', '$http', 'logger', '$state','cliente',
              function($scope, $http, logger, $state, cliente) {
	window.scope = $scope;
	$scope.cliente = cliente;
	
	$scope.consultaCep = function(){
		//https://viacep.com.br/ws/01001000/json/
		//https://viacep.com.br/ws/01001000/json/?callback=callback_name
		$http.jsonp('api://https://viacep.com.br/ws/'+  $scope.cliente.endereco.cep + '/json/?callback=JSON_CALLBACK').then(
			function(response){
				if(response.status == 200){
					if(response.data.hasOwnProperty("erro")){
						logger.logWarning("Cep não encontrado");
					}
					$scope.cliente.endereco.logradouro = response.data.logradouro;
					$scope.cliente.endereco.bairro = response.data.bairro;
					$scope.cliente.endereco.cidade = response.data.localidade;
					$scope.cliente.endereco.uf = response.data.uf;
					$scope.cliente.endereco.complemento = response.data.complemento;
				}
		})
	}
	

	
	$scope.cadastrarCliente = function(){
		var params = {
		};
		$http.post( "clientes/cadastro/new" , $scope.cliente).then(
			function(response){
				if(response.status == 200){
					logger.logSuccess('Cliente cadastrado com Sucesso');
					$state.go("cliente")
				} else {
					logger.logError("Erro ao cadastrar Cliente")
				}
			}
			);
	}

}]);
app.controller('ClienteController', ['$scope', '$http', 'clientes', 'logger', function($scope, $http, clientes, logger) {
	window.scope = $scope;
	
	$scope.listCliente = clientes.resultado.listClientes;
	
	
	$scope.deleteCliente = function(cliente){
		$http.delete('clientes/delete/' + cliente.id).then(function(response){
			if(response.status == 200){
				logger.logSuccess('Usuário deletado com sucesso');
				$scope.listCliente.splice($scope.listCliente.indexOf(cliente),1);
			}
		})
	}
}]);
app.controller('ConsultaFornecedorController', ['$scope', '$http', 'logger', 'listaFornecedor', function($scope, $http, logger, listaFornecedor){
	window.scope = $scope;

	$scope.listFornecedor = listaFornecedor;

	$scope.deletarFornecedor = function(fornecedor){
		$http.delete('fornecedor/delete/' + fornecedor.id).then(function(response){
			if(response.status == 200){
				logger.logSuccess('Fornecedor Deletado com sucesso');
				$scope.listaFornecedor.splice($scope.listaFornecedor.indexOf(fornecedor),1);
			}
		})
	}
	
}]);
app.controller('FornecedorController', ['$scope', '$http', 'responseCtrl', 'logger', '$state',  'fornecedor',
	function($scope, $http, responseCtrl, logger,$state,fornecedor){
	window.scope = $scope;
	$scope.fornecedor = {};


	
	$scope.cadastrarFornecedor = function(){
		var params = {
		};
		$http.post("fornecedor/cadastro/new", $scope.fornecedor).then(
			function(response){
				if(response.status == 200){
					logger.logSuccess('Fornecedor Cadastrado com Sucesso');
					$state.go("fornecedor/consulta")
				}else{
					logger.logError("Erro ao cadastrar Produto")
				}
			}	
		);
	}
	
}]);
app.controller('CadastroTipoOcorrenciaController', ['$scope', '$state' , 'treeview', '$uibModal',  function($scope, $state, treeview, $uibModal){
	var tree;
	window.scope = $scope;
	console.log(treeview);
	$scope.my_tree = tree = {};
	$scope.treeview = treeview;
	$scope.selected = null;
	$scope.modal = {};

	console.log(tree);

	$scope.selectedBranch = function(branch){
		$scope.selected = branch;
	}


	 $scope.modal.item = {
        'open': function (branch,edit) {
            $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title-top',
                ariaDescribedBy: 'modal-body-top',
                templateUrl: 'views/gerenciamento/popupTipo.html',
                size: 'md',
                controller: 'CadastroItemController',
                resolve: {
                    'itemPai': branch,
                    'edit': edit
                }
            });
        }
	}

	 return $scope.try_adding_a_branch = function() {
      var b;
      b = tree.get_selected_branch();
      console.log("essa é a branch selecionada", b);
      return tree.add_branch(b, {
        label: 'New Branch',
        data: {
          something: 42,
          "else": 43
        }
      }, function(b){
      	console.log(b);
      });
    };


}]);

app.controller('CadastroItemController', ['$scope', '$uibModalInstance', 'itemPai', 'logger', '$http', 'UsuarioService', '$state', 'edit', function ($scope, $uibModalInstance, itemPai, logger, $http, UsuarioService, $state, edit) {

	// $scope.area = UsuarioService.area().then(function(response){
	// 	return response;
	// });

	UsuarioService.area().then(function(result){
		$scope.area = result;
	})
	console.log(itemPai);
	$scope.edit = edit;
	window.scope = $scope;
	if(itemPai != null){
		$scope.item = {
		'id': null,
		'nmeTipo': null,
		'sla': null,
		'parent':{
			'id': itemPai.id
		},
		'area':{
			'id':null
		}
	};	
	} else {
		$scope.item = {
		'id': null,
		'nmeTipo': null,
		'sla': null,
		'area':{
			'id':null
		}
	};
	}

	if(edit == true){
		$scope.item = itemPai;
		$scope.item.nmeTipo = itemPai.label;
		$scope.item.area = {
			'id': String(itemPai.area.id)
		}
	}
	
	
	$scope.itemPai = itemPai;
	console.log($scope.itemPai);
    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.salvar = function(){
        // var historico = {
        //     'id': null,
        //     'ocorrencia': {
        //         'id': ocorrencia.id
        //     },    
        //     'acao':{
        //         'id': null,
        //         'comentario': $scope.comentario
        //     }
        // }
        $http.post('ocorrencia/arvore/item/new', $scope.item).then(function(response){
            if(response.status == 200){
                logger.logSuccess('item Cadastrado com sucesso');
                $uibModalInstance.dismiss('cancel');
                $state.reload();
            }
        })
    }

}]);
app.controller('LoginController', ['$scope','$http','$location', function($scope,$http, $location ){
	window.scope = $scope;
	$scope.pageComplete = true;
	$scope.usuario = {};
	

	var config = {
		headers:  {
		'Content-Type': 'application/x-www-form-urlencoded'
		}
    };

	$scope.efetuarLogin = function(){
		$http.post('login', $.param($scope.usuario),config)
			.success(
				function(data, status, headers, config){
					localStorage.setItem("tokenApe",headers('X-AUTH-TOKEN') );
					$location.path("/dashboard");
				}
			)
			.error(
				function(data, status, headers, config){
					console.log(data);
				}
			);
	}

}]);

app.controller('MenuController', ['$scope', '$location', function($scope, $location) {
	 $scope.isActive = function (viewLocation) {
		 viewLocation = viewLocation.split(",")
		 for(i=0; i < viewLocation.length;i++){
			 if (viewLocation[i] == $location.path()){
				 return 'active'
			 }
		 }
		
	        //return viewLocation === $location.path();
	    };
}]);
app.controller('GerenciamentoOcorrenciaController', ['$scope', '$http', '$location', '$state', 'logger', function($scope,$http, $location, $state, logger) {
	
	$scope.processClose = function(){
		$http.get("ocorrencia/finished").then(function(response){
			if(response.status == 200){
				$scope.listaFinalizada = response.data.resultado.ocorrencias;
			}
		})
	}

	

}]);
app.controller('ComentarioController', ['$scope', '$uibModalInstance', 'ocorrencia', 'logger', '$http', function ($scope, $uibModalInstance, ocorrencia, logger, $http) {

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.addComentario = function(){
        var historico = {
            'id': null,
            'ocorrencia': {
                'id': ocorrencia.id
            },    
            'acao':{
                'id': null,
                'comentario': $scope.comentario
            }
        }

        $http.post('ocorrencia/comentario', historico).then(function(response){
            if(response.status == 200){
                logger.logSuccess('Comentario adicionado na ocorrência Nº: ' + ocorrencia.id);
                $uibModalInstance.dismiss('cancel');
            }
        })
    }

}]);

app.controller('OcorrenciaStatusController', ['$scope', '$uibModalInstance', 'ocorrencia', 'logger', '$http', '$state', 'selectedStatus', function ($scope, $uibModalInstance, ocorrencia, logger, $http, $state, selectedStatus) {

    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.status = selectedStatus;
  

    $scope.action = function(){
         var historico = {
            'id': null,
            'ocorrencia': {
                'id': ocorrencia.id
            },    
            'acao':{
                'id': null,
                'comentario': $scope.comentario
            },
            'status': {
                'id':$scope.status.id
            }
        }

        $http.post('ocorrencia/changeStatus', historico).then(function(response){
            if(response.status == 200){
                logger.logSuccess('Mudança de Status realizada na ocorrência Nº: ' + ocorrencia.id);
                $uibModalInstance.dismiss('cancel');
                $state.reload();
            }
        })
    }

}]);

app.controller('AdicionaServicoController', ['$scope', '$uibModalInstance', 'ocorrencia', 'logger', '$http','$state', function ($scope, $uibModalInstance, ocorrencia, logger, $http, $state) {

    $scope.servicos = [];


    $http.get('servico/listAllServicos').then(
        function(response){
            if(response.status == 200){
                $scope.servicos = response.data.resultado.listServicos;
                console.log($scope.servicos);
            }
        }
    )
    


    $scope.cancel = function () {
        $uibModalInstance.dismiss('cancel');
    }

    $scope.addServico = function(){
        var servico = {
            'id': null,
            'ocorrencia': {
                'id': ocorrencia.id
            },    
            'servico':{
                'id': $scope.servico
            }
        }

        $http.post('ocorrencia/servico', servico).then(function(response){
            if(response.status == 200){
                logger.logSuccess('Adicionado serviço na ocorrência Nº: ' + ocorrencia.id);
                $uibModalInstance.dismiss('cancel');
                $state.reload();
            }
        })
    }

}]);



app.controller('TratarOcorrenciaController', ['$scope', '$http', '$state', 'logger', '$uibModal', 'ocorrencia', 'OcorrenciaService','status', function ($scope, $http, $state, logger, $uibModal, ocorrencia, OcorrenciaService, status) {
    console.log('oc', ocorrencia);
    $scope.ocorrencia = ocorrencia;
    $scope.modal = {};
    $scope.ordemServico = {};
    $scope.listaStatus = status;
    $scope.statusAtual = ocorrencia.status.id; 
    $scope.sameUser = false;

    window.scope = $scope;

    if($scope.userInfo.username == $scope.ocorrencia.usuario.usuario){
        $scope.sameUser = true;
    }

    $scope.init = function(){
        $scope.ordemServico.total = 0;
        $scope.ordemServico.totalAprovado = 0;
        $scope.ordemServico.faltaAprovar = 0;
        for(var i = 0; i < $scope.ocorrencia.servicos.length; i++){
            var servico = $scope.ocorrencia.servicos[i].servico;
            $scope.ordemServico.total += servico.valor;
            if(scope.ocorrencia.servicos[i].aprovado == true){
              $scope.ordemServico.totalAprovado +=  servico.valor;
            } else {
                $scope.ordemServico.faltaAprovar += 1;
            }
        }
    }

    $scope.modal.comentario = {
        'open': function () {
            $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title-top',
                ariaDescribedBy: 'modal-body-top',
                templateUrl: 'views/ocorrencia/comentario.html',
                size: 'md',
                controller: 'ComentarioController',
                resolve: {
                    'ocorrencia': $scope.ocorrencia
                }
            });
        }
    }

    $scope.modal.servico = {
        'open': function () {
            $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title-top',
                ariaDescribedBy: 'modal-body-top',
                templateUrl: 'views/ocorrencia/servico.html',
                size: 'md',
                controller: 'AdicionaServicoController',
                resolve: {
                    'ocorrencia': $scope.ocorrencia
                }
            });
        }
    }




   

    $scope.aprovarService = function(id){
        $http.get('ocorrencia/servico/aprovar/' + id).then(function(response){
            if(response.status == 200){
                logger.logSuccess("Serviço Aprovado");
                $state.reload();
            }
        })

    }


    $scope.excludeService = function(id){
        $http.delete('ocorrencia/servico/' + id).then(function(response){
            if(response.status == 200){
                logger.logSuccess("Serviço deletado");
                $state.reload();   
            }
        })
         
    }

   
    $scope.modal.status = {
        'open': function (selectedStatus) {
             this.modal_f = $uibModal.open({
                animation: true,
                ariaLabelledBy: 'modal-title-top',
                ariaDescribedBy: 'modal-body-top',
                templateUrl: 'views/ocorrencia/finalizacao.html',
                size: 'md',
                controller: 'OcorrenciaStatusController',
                resolve: {
                    'ocorrencia': $scope.ocorrencia,
                    'selectedStatus': selectedStatus
                }
            });
        },
        'close': function(){
            console.log("fechou", this.modal_f);
        }
    }

    $scope.reabrir = function(){
        var status =  {
            'id': 5,
            'nmeStatus': 'REABERTO'
        }
        $scope.modal.status.open(status);
    }

    $scope.atribuirForMe = function(){
        $http.get('ocorrencia/me' + id).then(function(response){
            if(response.status == 200){
                $state.reload();
            }
        })
    }

    

    function mountEvent (event){
        var icon = '';
        var acao = '';
        var noText = true;
        if(event.acao.tipoAcao == 'CRIACAO'){
            acao = 'criou a ocorrência';
            icon = 'fa fa-circle bg-blue';
            noText = true;
        }
        if(event.acao.tipoAcao == 'COMENTARIO'){
            acao = 'comentou na ocorrência';
            icon = 'fa fa-comments bg-yellow';
            noText = false;
        }
        if(event.acao.tipoAcao == "MUDANCA_DE_STATUS"){
            acao = 'mudou o status da ocorrência para ' +  event.status.nmeStatus;
            icon = 'fa fa-code-fork';
            if(event.acao.comentario != "" && event.acao.comentario != null)
                noText = false;   
        }

        event.date = moment(event.acao.dtaAcao, 'DD-MM-YYYY HH:mm:ss').format("DD.MM.YYYY");
        event.hour = moment(event.acao.dtaAcao, 'DD-MM-YYYY HH:mm:ss').format("HH:mm:ss");
        event.ago = moment(event.acao.dtaAcao, 'DD-MM-YYYY HH:mm:ss').fromNow(); 
        event.icon = icon;
        event.action = acao;
        event.noText = noText; 
        return event;
    }
    var handleSuccess = function(data, status) {
        $scope.line = data.resultado;
        console.log($scope.line);
        $scope.timeline = [];
        for(var i=0; i < $scope.line.actions.length; i++){
            $scope.line.actions[i] = mountEvent($scope.line.actions[i]);
            if(i == 0){
                $scope.timeline.push([ $scope.line.actions[i] ]);
            } else {
                if( $scope.line.actions[i].date ==  $scope.line.actions[i-1].date){
                    $scope.timeline[$scope.timeline.length -1].push($scope.line.actions[i])
                } else{
                    $scope.timeline[$scope.timeline.length] = [$scope.line.actions[i]];
                }
            }
        }
        console.log("TIME", $scope.timeline);
    };

    $scope.atualizarTimeLine = function(){
        console.log('atualizando');
        OcorrenciaService.getActions($scope.ocorrencia.id).success(handleSuccess);
    }


}]);




app.controller('ConsultaSegmentoController', ['$scope', '$http', 'logger', 'listSegmentos', 
	function($scope, $http, logger, listSegmentos){
	window.scope = $scope;

	$scope.listSegmento = listSegmentos;



	$scope.deleteSegmento = function(segmento){
		$http.delete('segmento/delete/' + segmento.id).then(function(response){
			if(response.status == 200){
				logger.logSuccess('Segmento deletado com sucesso');
				$scope.listSegmento.splice($scope.listSegmento.indexOf(segmento),1);
			}
		})
	}
	
}]);
app.controller('SegmentoController', ['$scope', '$http', 'segmento', 'logger', '$state',function($scope, $http, segmento, logger,$state){
	window.scope = $scope;
	$scope.segmento = segmento;



	
	$scope.salvarSegmento = function(){
		var params = {
		};
		$http.post("segmento/cadastro/new", $scope.segmento).then(
			function(response){
				if(response.status == 200){
					logger.logSuccess('Segmento Cadastrado com Sucesso');
					$state.go("segmento/consulta")
					
				}else{
					logger.logError("Erro ao cadastrar Segmento")
				}
			}	
		);
	}
	
}]);
app.controller('CadastroServicoController', ['$scope', '$http', 'logger', 'servico', '$state','listarAllTipos','listarAllFornecedor',
	function($scope, $http, logger,servico,$state, listarAllTipos,listarAllFornecedor){
	window.scope = $scope;
	

	$scope.servico = servico;


	$scope.listAllTipo = listarAllTipos;
	$scope.listFornecedor = listarAllFornecedor;


	
	$scope.salvarServico = function(){
		$http.post("servico/cadastro/new", $scope.servico).then(
			function(response){
				if(response.status == 200){
					logger.logSuccess('Serviço Cadastro com Sucesso');
					$state.go("servico/consulta")
				}else{
					logger.logError("Erro ao cadastrar Serviço")
				}
			}	
		);
	}
	
}]);
app.controller('servicoController', ['$scope', '$http', 'logger', 'listaServico', function($scope, $http, logger, listaServico){
	window.scope = $scope;

	    //console.log(servico);


	
	console.log(listaServico);
	$scope.listaServico= listaServico;

	$scope.deletarServico = function(servico){
		$http.delete('servico/delete/' + servico.id).then(function(response){
			if(response.status == 200){
				logger.logSuccess('Serviço deletado com sucesso');
				$scope.listaServico.splice($scope.listaServico.indexOf(servico),1);
			}
			}	
		);
	}
	
}]);
app.controller('tipoServicoConsulta', ['$scope', '$http', 'logger', 'listarAllTipos', function($scope, $http, logger, listarAllTipos){
	window.scope = $scope;

	$scope.listAllTipo = listarAllTipos

	$scope.deletarTipoServico = function(tipoServico){
		$http.delete('tipoServico/delete/' + tipoServico.id).then(function(response){
			if(response.status == 200){
				logger.logSuccess('Tipo deletado com sucesso');
				$scope.listAllTipos.splice($scope.listAllTipos.indexOf(listAllTipos),1);
			}
		})
	}
	
}]);
app.controller('tipoServicoController', ['$scope', '$http', 'logger', '$state', 'listOne',function($scope, $http, logger,$state,listOne){
	window.scope = $scope;
	$scope.tipoServico = listOne;



	
	$scope.salvartipoServico = function(){
		var params = {
		};
		$http.post("tipoServico/cadastro/new", $scope.tipoServico).then(
			function(response){
				if(response.status == 200){
					logger.logSuccess('Unidade de Medida Cadastrada com Sucesso');
					$state.go("tipoServico/consulta")
				}else{
					logger.logError("Erro ao cadastrar Unidade de Medida")
				}
			}	
		);
	}
	
}]);
app.controller('CadastroCorController', ['$scope', '$http', 'logger','$state', 'cor', function($scope, $http, logger, $state, cor){
	window.scope = $scope;
	
	console.log(cor);
	$scope.cor = cor;


	$scope.cadastrarCor = function(){
		$http.post("veiculo/cadastroCor/new", $scope.cor).then(
			function(response){
				if(response.status == 200){
					logger.logSuccess('Cor cadastrada');
					$state.go('veiculo/consultaCor');
				}else{
					logger.logError("Erro ao cadastrar Cor")
				}
			}	
		);
	}
	
}]);
app.controller('CadastroMarcaController', ['$scope', '$http', 'marca', 'logger', '$state', function($scope, $http, marca, logger,$state){
	window.scope = $scope;
	$scope.marca = marca;

	
	$scope.salvarVeiculoMarca = function(){
		var params = {
		};
		$http.post( "veiculo/cadastroVeiculoMarca/new" , $scope.marca).then(
			function(response){
				if(response.status == 200){
					logger.logSuccess('Marca salva.');	
					$state.go('veiculo/consultaMarca');	
				} else {
					logger.logError("Erro ao cadastrar Marca")
				}
			}
			);
	}
	
}]);
app.controller('CadastroModeloController', ['$scope', '$http', 'responseCtrl', 'logger','modelo', '$state', function($scope, $http, responseCtrl, logger,modelo ,$state){
	window.scope = $scope;
	$scope.modelo = modelo;
	$scope.listMarcas = responseCtrl.data.listMarcas;

	
	$scope.cadastrarModelo = function(){
		$http.post("veiculo/cadastroVeiculoModelo/new", $scope.modelo).then(
			function(response){
				if(response.status == 200){
					logger.logSuccess('Modelo cadastrado');
					$state.go('veiculo/consultaModelo');	
				}else{
					logger.logError("Erro ao cadastrar Modelo")
				}
			}	
		);
	}
	
}]);
app.controller('CadastroVeiculoController', ['$scope', '$http', 'responseCtrl', 'logger','listarCores','todosClientes','veiculo','$state' ,
              function($scope, $http, responseCtrl, logger, listarCores, todosClientes, veiculo, $state){
	window.scope = $scope;
	$scope.veiculo = veiculo.veiculoCliente;
	$scope.cor = {};
	$scope.veiculoCliente= {};
	$scope.listMarcas = responseCtrl.data.listMarcas;
	//$scope.listClientes = todosClientes.listClientes;
	$scope.listCliente = todosClientes.resultado.listClientes;
	$scope.listCor = listarCores.listCor;
	
	
	$scope.cadastrarVeiculoCliente = function(){
		var params = {
		};
		$http.post( "veiculo/cadastro/new" , $scope.veiculo).then(
			function(response){
				if(response.status == 200){
					logger.logSuccess('Veiculo salvo.');	
					$state.go('veiculo/consultaVeiculo');		
				} else {
					logger.logError("Erro ao cadastrar Cliente")
				}
			}
			);
	}
	
	$scope.changeMarca = function(){
		$scope.listModelos = [];
		if($scope.veiculo.veiculoMarca != "" && typeof $scope.veiculo.veiculoMarca != "undefined" ){
			$http.get('veiculo/listModelos?idMarca=' + $scope.veiculo.veiculoMarca.id).success(
					function(data){
					$scope.listModelos = data.listModelo;
				})
		}
	}
	
	$scope.changeMarca();
}]);
app.controller('consultaMarcaController', ['$scope', '$http', 'responseCtrl', 'logger',  function($scope, $http, responseCtrl, logger){
	window.scope = $scope;

	
	$scope.listMarcas = responseCtrl.data.listMarcas;

$scope.deletarMarca = function(veiculoMarca){
		$http.delete('veiculo/deleteMarca/' + veiculoMarca).then(function(response){
			if(response.status == 200){
				logger.logSuccess('Marca deletada com sucesso');
				$scope.listMarcas.splice($scope.listMarcas.indexOf(veiculoMarca),1);
			}
			}	
		);
	}
	
	
	
}]);
app.controller('consultaModeloController', ['$scope', '$http', 'responseCtrl', 'logger', function($scope, $http, responseCtrl, logger){
	window.scope = $scope;

	
	$scope.veiculoModelo= {};
	$scope.listTodosModelos = responseCtrl.data.listTodosModelos;


	$scope.deleteModelo = function(veiculoModelo){
		$http.delete('veiculo/deleteModelo/' + veiculoModelo).then(function(response){
			if(response.status == 200){
				logger.logSuccess('Modelo deletado com sucesso');
				$scope.listTodosModelos.splice($scope.listTodosModelos.indexOf(veiculoModelo),1);
			}
			}	
		);
	}
	
	
	
}]);
app.controller('corController', ['$scope', '$http', 'responseCtrl', 'logger', function($scope, $http, responseCtrl, logger){
	window.scope = $scope;

	
	$scope.veiculoCor= {};
	$scope.listarCor = responseCtrl.listCor;

	
	
	$scope.deleteCor = function(veiculoCor){
		$http.delete('veiculo/deleteCor/' + veiculoCor).then(function(response){
			if(response.status == 200){
				logger.logSuccess('Cor deletado com sucesso');
				$scope.listarCor.splice($scope.listarCor.indexOf(veiculoCor),1);
			}
			}	
		);
	}
	
	
}]);
app.controller('veiculoController', ['$scope', '$http', 'responseCtrl', 'logger', function($scope, $http, responseCtrl, logger ){
	window.scope = $scope;

	$scope.veiculoCliente= {};
	$scope.listarVeiculoCliente = responseCtrl.listVeiculoCliente;

	
	
	$scope.deleteVeiculoCliente = function(veiculoCliente){
		$http.delete('veiculo/delete/' + veiculoCliente).then(function(response){
			if(response.status == 200){
				logger.logSuccess('Veiculo deletado com sucesso');
				$scope.listarVeiculoCliente.splice($scope.listarVeiculoCliente.indexOf(veiculoCliente),1);
			}
		})
	}
	
}]);