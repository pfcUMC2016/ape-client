/* MEU APP JS - ANGULAR */


window.onerror = function(message, source, lineno, colno, error) { 
alert("ERRO NA PAGINA")
};

var PATH = "/ape/";
var WEBSERVICE = window.location.origin + ":8080/ape/";


var app = angular.module('ape', ['ui.router', 'app.services', 'ui.mask','isteven-multi-select', 'angularBootstrapNavTree', 'ui.bootstrap', 'angularMoment', 'angular-loading-bar']);


app.config(['cfpLoadingBarProvider', function(cfpLoadingBarProvider) {
    //cfpLoadingBarProvider.parentSelector = '.loader-ape';
  }]);


app.controller('DashBoardController', ['$scope', '$http','$rootScope', function($scope, $http, $rootScope){
	

	
}])


app.controller('MenuController', ['$scope', '$http','$state', '$location', function($scope, $http, $state, $location){
	$scope.newAtendimento = function(){
		$http.get('atendimento/new').then(function(response){
			if(response.status == 200){	
				$location.path("atendimento/" + response.data.resultado.atendimento);
			}
		})
	}
}])


app.controller('ResourceController', ['$scope', '$http', '$location','$window', '$rootScope', '$state', 'logger', function($scope,$http, $location, $window, $rootScope, $state, logger) {
	window.scope = $rootScope;
$scope.pageComplete = true;

	moment.locale('pt-BR');

	$rootScope.$on('$stateChangeStart', function(event, toState, toParams, fromState, fromParams){
		$scope.pageComplete = false;
		var u = toState;
		if(u.url == "/login" || u.name.indexOf("atendimento") != -1 ){
				$scope.especifico = true;
		}else {
			$scope.especifico = false;
		}
		$scope.resource();
		$scope.pageComplete = true;
	});
	

	 $scope.isActive = function (viewLocation) {
		 viewLocation = viewLocation.split(",")
		 for(i=0; i < viewLocation.length;i++){
			 if (viewLocation[i] == $location.path()){
				 return 'active'
			 }
		 }
	    };
	
	$scope.resource = function(){
		$http.get( '/resource').then(function(response){
			if(response.status == 200){
				console.log("Sucesso: ", response	.data);
				$scope.userInfo = response.data;
				$scope.pageComplete = true;
				if($location.path() == "/login"){
					$location.path('/dashboard');
				}
				$rootScope.roles =  $scope.userInfo.roles;
				

				$rootScope.listPermissoes = "";
				$rootScope.isAdmin = false;
				var roles = $rootScope.roles;
				for(var i=0; i < roles.length; i++){
					$rootScope.listPermissoes +=  " " + roles[i].role + " ";
					if(roles[i].role == "ROLE_ADMIN"){
						$rootScope.isAdmin = true;
					}
				}
				console.log($rootScope.listPermissoes);
				if($rootScope.isAdmin == true && $location.path() == "/dashboard" ){
					console.log("TO NO DASHBOARD");
					$http.get('dashboard').then(function(response){
						$scope.result = response.data.resultado;
						console.log($scope.result);
						//console.log(result);
					})
				}
			} else {
				$scope.especifico = true;
				$state.go('login');
				$scope.pageComplete = true;
			}
		})
	}



	$scope.abrirOcorrencia = function(id){
		console.log('click');
		$state.go("ocorrencia/:idOcorrencia", {idOcorrencia:id} );
		//$state.go("cliente/cadastro/:idCliente", {idCliente:2});
	}

	$scope.finalizarOcorrencia = function(e,ocorrencia){
		var param = {
			'id': ocorrencia.id
		};
		$http.post("ocorrencia/encerrar",param).then(function(response){
			if(response.status == 200){
				logger.logSuccess("Ocorrencia Nº: " + ocorrencia.id + " foi finalizada com sucesso!");
				$state.reload();
			}
		})
	}

	$scope.logout = function(){
		localStorage.removeItem("tokenApe");
		$window.location = '/ape/#login';
	}

	//openMenu();
//	$scope.resource();
}]);





