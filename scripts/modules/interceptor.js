//(function(){
	
var app = angular.module('app.services', []);

app.factory('TokenService', [  function() {
    return {
    	getToken: function(){
    		var token = "";
    	    if (window.localStorage.hasOwnProperty("tokenApe")) {
    	    	token = localStorage.getItem("tokenApe");
    	    }

    	    return token;
    	}
    }
}]);


app.factory('httpInterceptor', [ '$q','TokenService', '$window', '$injector', 'logger',  function($q,TokenService,$window,  $injector, logger) {
//angular.module('app', []).factory('httpInterceptor', httpInterceptor);

//httpInterceptor.$inject = ['$q', 'getToken'];

//function Interceptor($q, getToken){
	return {

		request: function (config) {
            if( config.url.indexOf("http://") == -1 || config.url.indexOf("https://") == -1){
				if(config.url.indexOf("api://") != -1){
					config.url = config.url.replace("api://", "");
				} else if (config.url.indexOf(".html") == -1){
					config.url = WEBSERVICE + config.url;
                } else {
                    config.url = PATH + config.url;
                }
            }   
        
            config.headers['X-AUTH-TOKEN'] = TokenService.getToken();
			config.headers['Accept-Language'] = 'pt-BR';
        	return config || $q.when(config)
        },

        response: function (response) {
        	return response || $q.when(response);
        },

        responseError: function (response) {
        	if (response.status == 401){
                $injector.get('$state').transitionTo('401');
			}
            if(response.status == 400){
                if(response.data.hasOwnProperty("errors")){
                    for(var i=0; i < response.data.errors.length; i++){
                        logger.logWarning(response.data.errors[i]);
                    }
                }
            }
            if( (response.status == 403 || response.status == 500) && response.data.hasOwnProperty('mensagem')){
                logger.logError("[Erro]: " + response.data.mensagem);
            }
        	return $q.reject(response);
        }

	}
}]);
