'use strict';


app.factory('AtendimentoService', ['$http', '$q', '$timeout', '$state', function($http, $q, $timeout, $state){
	return {
		returnAtendimento: function(){
			return $http.get('atendimento/returnAtendimento').then(
				function(response){
					return response.data.resultado.atendimento;
						
						
				},
				function(errResponse){
					console.log("erro");
					return $q.reject(errResponse);
				}
			)
		},
		getAtendimento: function(idAtendimento) {
			return $http.get("atendimento/"+idAtendimento).then(
				function(response){
					return response.data.resultado.atendimento;
				},
				function(errResponse){
					console.log("erro");
					return $q.reject(errResponse);
				}

			)
		}
	}
}])

app.factory ('FornecedorService', ['$http', '$q', '$timeout', function($http, $q, $timeout){
	return {
		
		listarAllFornecedor: function(){
			return $http.get('fornecedor/listAllFornecedor').then(
				function(response){
				return response.data.resultado.listFornecedor;
			},
			function(errResponse){
				return $q.reject(errResponse);
			}
			);
		},

oneFornecedor: function(id){
			var idFornecedor = id;
			return $http.get('fornecedor/' + idFornecedor).then(
					function(response){
						return response.data.resultado.fornecedor;
					},
					function(errResponse){
						console.log('ERRO');
						return $q.reject(errResponse);
					}
			);
		}

	}
}])

app.factory ('UsuarioService', ['$http', '$q', '$timeout', function($http, $q, $timeout){
	return {
		
		allPermissoes: function(){
			return $http.get('usuario/listPermissoes').success(
				function(data){
					return data.listaPermissoes;
				}
			);
		},
		allUsers: function(){
			return $http.get('usuario/listUsuarios').then(
					function(response){
					 	return response.data.resultado.listaUsuario;
					 },
					 function(errResponse){
					 	return $q.reject(errResponse);
					 }
			);
		},
		area: function(){
			return $http.get('usuario/allArea').then(
				function(response){
					return response.data.resultado.areas;
				},
				function(errResponse){
					return $q.reject(errResponse);
				}
			);
		},
	}
}])



app.factory ('SegmentoService', ['$http', '$q', '$timeout', function($http, $q, $timeout){
	return {
		
		listarAllSegmentos: function(){
			return $http.get('segmento/listAllSegmentos').then(
				function(response){
				return response.data.resultado.listSegmentos;
			},
			function(errResponse){
				return $q.reject(errResponse);
			}
			);
		},
		oneSegmento: function(id){
			var idSegmento = id;
			return $http.get('segmento/' + idSegmento).then(
					function(response){
						return response.data.resultado.segmento;
					},
					function(errResponse){

						console.log('ERRO');
						return $q.reject(errResponse);
				}
			);
		},

	}
}])



app.factory ('ServicoService', ['$http', '$q', '$timeout', function($http, $q, $timeout){
	return {
		
		listarAllServicos: function(){
			return $http.get('servico/listAllServicos').then(
				function(response){
					return response.data.resultado.listServicos;
				}
			);
		},

		findByService: function(servico){
			return $http.get('servico/cadastro/'+ servico_id).then(
				function(response){
					return response.data.resultado;
				},
				function(errResponse){

							}
			);
		},


		oneServico: function(id){
			var idServico = id;
			return $http.get('servico/' + idServico).then(
					function(response){
						return response.data.resultado.servico;
					},
					function(errResponse){

						console.log('ERRO');
						return $q.reject(errResponse);
				}
			);
		}

	}
}])


app.factory ('TipoServicoService', ['$http', '$q', '$timeout', function($http, $q, $timeout){
	return {
		
		listarAllTipos: function(){
			return $http.get('tipoServico/listAllTipo').then(
				function(response){
					return response.data.resultado.listtAllTipoServico;
				}
			);
		},
		listOneTipo: function(id){
			var idTipo = id;
			return $http.get('tipoServico/' + idTipo).then(
					function(response){
						return response.data.resultado.tipoServico;
					},
					function(errResponse){

						console.log('ERRO');
						return $q.reject(errResponse);}
			);
		}

	}
}])


app.factory('VeiculoService', ['$http', '$q', '$timeout', '$state', function($http, $q, $timeout, $state){
	return {
		todasMarcas: function(){
			return $http.get('veiculo/listMarcas').success(
					function(data){
						return data.listMarcas;
			});
		},
		oneMarca: function(id){
			var idMarca = id;
			return $http.get('veiculo/marca/' + idMarca).then(
				function(response){
					return response.data.resultado.marca;
				},
				function(errResponse){
					console.log('Erro');
				});
		},
		todosModelos: function(){
			return $http.get('veiculo/listTodosModelos').success(
					function(data){
						return data.listTodosModelos;
			});
		},
		oneModelo: function(id){
			var idModelo = id;
			return $http.get('veiculo/modelo/' + idModelo).then(
				function(response){
					return response.data.resultado.modelo;
				},
				function(errResponse){
					console.log('Erro');
				});
		},
		allColors: function(){
			return $http.get('veiculo/listCor').then(
					function(response){
						return response.data;
					},
					function(errResponse){
						console.error('Error while fetching Items');
						return $q.reject(errResponse);
					}
			);
		},
		oneColor: function(id){
			var idCor = id;
			return $http.get('veiculo/cor/' + idCor).then(
				function(response){
					return response.data.resultado.cor;
				},
				function(errResponse){
					console.log('Erro');
				});
		},
		allVeiculos: function(){
			return $http.get('veiculo/listarVeiculos').then(
					function(response){
						return response.data;
					});
		},
		oneVeiculo: function(id){
			var idVeiculoCliente = id;
			return $http.get('veiculo/' + idVeiculoCliente).then(
				function(response){
					return response.data.resultado;
				},
				function(errRespose){
					console.log('Erro');
				});
		},
		byCliente: function(idAtendimento){
			return $http.get('veiculo/fromCliente/'+ idAtendimento).then(
				function(response){
					return response.data.resultado.veiculos;
				},
				function(errResponse){
					$q.reject(errResponse);
				}
			)
		}
	}
}])

app.factory('ClienteService', ['$http', '$q', '$timeout',"$state", function($http, $q, $timeout,$state){
	return {

		findByName: function(nome){
			return $http.get('clientes/pesquisa/'+ nome).then(
				function(response){
					return response.data.resultado;
				},
				function(errResponse){
						console.log('ERRO');
						return $q.reject(errResponse);
					}
			);
		},
		
		newUserReturn: function(){
			return $http.get('usuario/returnNewUser').success(
				function(data){
					return data;
				}
			);
		},
		
		allClients: function() {
			return $http.get('clientes/listClientes').then(
					function(response){
						return response.data;
                    },
                    function(errResponse){
                    	console.error('Error while fetching Items');
                    	return $q.reject(errResponse);
                    }
            );
        },
		oneClient: function(id){
			var idCliente = id;
			return $http.get('clientes/' + idCliente).then(
					function(response){
						return response.data.resultado.cliente;
					},
					function(errResponse){
						console.log('ERRO');
						return $q.reject(errResponse);
					}
			);
		}
    };
}]);



app.factory('OcorrenciaService', ['$http', '$q', function($http, $q){
	return {
		"treeview": function(){
			return $http.get("ocorrencia/treeview").then(
				function(response){
					return response.data.resultado.treeview;
				},
				function(errResponse){
					console.log('ERRO');
					return $q.reject(errResponse);
				}
			);
		},
		"prioridade":function(){
			return $http.get("ocorrencia/prioridade").then(
				function(response){
					return response.data.resultado.prioridade;
				},
				function(errResponse){
					return $q.reject(errResponse);
				}
			);
		},
		"status": function(){
			return $http.get("ocorrencia/status").then(
				function(response){
					return response.data.resultado.status;
				},
				function(errResponse){
					return $q.reject(errResponse);
				}
			)
		},
		"historico": function(idAtendimento){
			 return $http.get("ocorrencia/historico/"+idAtendimento).then(
				function(response){
					return response.data.resultado.historico;
				},
				function(errResponse){
					return $q.reject(errResponse);
				}
			)
		},
		"getOcorrencia": function(idOcorrencia){
			 return $http.get("ocorrencia/"+idOcorrencia).then(
				function(response){
					return response.data.resultado.ocorrencia;
				},
				function(errResponse){
					return $q.reject(errResponse);
				}
			)
		},
		"getActions": function(idOcorrencia){
			 return $http.get("ocorrencia/"+idOcorrencia+"/actions/")
		},

		"changeStatus": function(action){
			return $http.post("ocorrencia/changeStatus", action).then(
				function(response){
					return response.data;
				},
				function(errResponse){
					return $q.reject(errResponse);
				}
			)
		}
	};
}]);
