
'use strict';

app.config(function($stateProvider, $httpProvider, $urlRouterProvider) { // $controllerProvider,

$stateProvider.state('cliente', {
		url:'/cliente',
		templateUrl : '/views/cliente/cliente.html',
		controller : 'ClienteController',
		resolve: {
			clientes : function(ClienteService){
				return ClienteService.allClients();
			}
		}
		
		
	})

.state('cliente/cadastro', {
		url:'/cliente/cadastro',
		templateUrl : '/views/cliente/cadastro.html',
		controller : 'CadastroClienteController',
		resolve: {
			cliente : function(){
				return null
			}
		}
	})

.state('cliente/cadastro/:idCliente', {
		url: '/cliente/cadastro/:idCliente',
		templateUrl : '/views/cliente/cadastro.html',
		controller : 'CadastroClienteController',
		resolve : {
			cliente : function(ClienteService,$stateParams) {
				return ClienteService.oneClient($stateParams.idCliente);
			}
		}
	})

.state({
		url: '/login',
		name: 'login',
		templateUrl :  '/views/login.html',
		controller : 'LoginController'
	})


.state({
		name: 'dashboard',
		url: '/dashboard',
		templateUrl :  '/views/dashboard.html',
		controller : 'DashBoardController'
	})


	.state('veiculo/consultaVeiculo', {
		url : '/veiculo/consultaVeiculo',		
		templateUrl : '/views/veiculo/consultaVeiculo.html',
		controller : 'veiculoController',
		resolve : {
			"responseCtrl" : function(VeiculoService) {
				return VeiculoService.allVeiculos();
			},
		}
	})

	.state('veiculo/consultaCor',{
		url: '/veiculo/consultaCor',
		templateUrl : '/views/veiculo/consultaCor.html',
		controller : 'corController',
		resolve : {
			"responseCtrl": function(VeiculoService) {
				return VeiculoService.allColors();
			}
		}
	})


	.state('veiculo/cadastroVeiculoModelo',{
		url : '/veiculo/cadastroVeiculoModelo',
		templateUrl : '/views/veiculo/cadastroVeiculoModelo.html',
		controller : 'CadastroModeloController',
		resolve : {
			"responseCtrl" : function(VeiculoService) {
				return VeiculoService.todasMarcas();
			},
			"modelo" : function() {
				return {}
			}
		}
	})

	

	.state('/404', {
		url: '/404',
		templateUrl : '/views/seguranca/404.html'
	})

	.state('401', {
		url : '/401',
		templateUrl : '/views/seguranca/401.html'
	})

	.state('ocorrencia/:idOcorrencia', {
		url: '/ocorrencia/:idOcorrencia',
		templateUrl : '/views/ocorrencia/ocorrencia.html',
		controller: 'TratarOcorrenciaController',
		resolve : {
			"ocorrencia" : function(OcorrenciaService, $stateParams) {
				return OcorrenciaService.getOcorrencia($stateParams.idOcorrencia);
			},
			"status" : function(OcorrenciaService){
				return OcorrenciaService.status();
			}
		}
	})

	

	.state('gerenciamento/ocorrencia', {
		url: '/gerenciamento/ocorrencia',
		controller:'GerenciamentoOcorrenciaController',
		templateUrl: '/views/ocorrencia/gerenciamento.html'
	})

	.state('gerenciamento/ocorrencia/tipo',{
		url: '/gerenciamento/ocorrencia/tipo',
		controller: 'CadastroTipoOcorrenciaController',
		templateUrl: '/views/gerenciamento/cadastroArvore.html',
		resolve:{
			"treeview" : function(OcorrenciaService) {
				return OcorrenciaService.treeview();
			}
		}
	})

	.state({
		url:'/atendimento/:idAtendimento',
		name: 'atendimento',		
		resolve: {
			"atendimento" : function(AtendimentoService, $stateParams){				
				return AtendimentoService.getAtendimento($stateParams.idAtendimento);	
			}
		},
		templateUrl : '/views/atendimento.html',
		controller : 'AtendimentoController'
	})

	.state('atendimento.finalizacao',{
		url: '/finalizacao',
		templateUrl : '/views/finalizacaoAtendimento.html',
		controller : 'FinalizacaoAtendimentoController'
	})


	.state('atendimento.ocorrencia',{
		url: '/ocorrencia',
		templateUrl : '/views/atendimento/ocorrencia.html',
		controller : 'AtendimentoOcorrenciaController',
		resolve : {
			"treeview" : function(OcorrenciaService) {
				return OcorrenciaService.treeview();
			},
			"prioridades" : function(OcorrenciaService){
				return OcorrenciaService.prioridade();
			},
			"status" : function(OcorrenciaService){
				return OcorrenciaService.status();
			},
			"historico": function(OcorrenciaService, $stateParams){
				return OcorrenciaService.historico($stateParams.idAtendimento);
			},
			"area": function(UsuarioService){
				return UsuarioService.area();
			},
			"veiculos": function(VeiculoService,$stateParams){
				return VeiculoService.byCliente($stateParams.idAtendimento);
			}
		}
	})



	.state('atendimento.associarCliente', {
		url: '/associarCliente',
		templateUrl: '/views/atendimento/associacaoCliente.html',
		controller: 'AssociarClienteController'
	})

	.state({
		url: '/usuario/cadastro',
		name: 'usuario/cadastro',
		templateUrl : '/views/usuario/cadastro.html',
		controller : 'CadastroUsuarioController',
		resolve : {
			"listPermissoes" : function(UsuarioService) {
				return UsuarioService.allPermissoes();
			},
			"listUsuarios" : function(UsuarioService) {
				return UsuarioService.allUsers();
			},
			"area": function(UsuarioService){
				return UsuarioService.area();
			}
		}
	})

	.state({
		url : '/veiculo/cadastro',
		name: 'veiculo/cadastro',
		templateUrl : '/views/veiculo/cadastro.html',
		controller : 'CadastroVeiculoController',
		resolve : {
			"responseCtrl" : function(VeiculoService) {
				return VeiculoService.todasMarcas();
			},
			"veiculo" : function(){
				return {
					'veiculoCliente': {}
				};
			},
			"todosClientes" : function(ClienteService) {
				return ClienteService.allClients();
			},
			"listarCores" : function(VeiculoService) {
				return VeiculoService.allColors();
			}

		}
	})
	
	.state({
		url :'/veiculo/cadastro/:idVeiculoCliente',
		name: 'veiculo/cadastro/unico',
		templateUrl : '/views/veiculo/cadastro.html',
		controller : 'CadastroVeiculoController',
		resolve : {
			"responseCtrl" : function(VeiculoService) {
				return VeiculoService.todasMarcas();
			},
			"veiculo" : function(VeiculoService, $stateParams){
				return VeiculoService.oneVeiculo($stateParams.idVeiculoCliente);
			},
			"todosClientes" : function(ClienteService) {
				return ClienteService.allClients();
			},
			"listarCores" : function(VeiculoService) {
				return VeiculoService.allColors();
			},
		}
	})

	.state({
		url: '/veiculo/cadastroCor/:idCor',
		name: 'veiculo/cadastroCor/unico',
		templateUrl: '/views/veiculo/cadastroCor.html',
		controller: 'CadastroCorController',
		resolve: {
			"cor" : function(VeiculoService, $stateParams){
				return VeiculoService.oneColor($stateParams.idCor);
			},
		}
	})

	.state({
		url: '/veiculo/cadastroMarca/:idMarca',
		name: 'veiculo/cadastroMarca/unico',
		templateUrl: '/views/veiculo/cadastroVeiculoMarca.html',
		controller: 'CadastroMarcaController',
		resolve: {
			"marca" : function(VeiculoService, $stateParams){
				return VeiculoService.oneMarca($stateParams.idMarca);
			},
		}
	})

	.state({
		url: '/veiculo/cadastroModelo/:idModelo',
		name: 'veiculo/cadastroModelo/unico',
		templateUrl: '/views/veiculo/cadastroVeiculoModelo.html',
		controller: 'CadastroModeloController',
		resolve: {
			"responseCtrl" : function(VeiculoService) {
				return VeiculoService.todasMarcas();
			},
			"modelo" : function(VeiculoService, $stateParams){
				return VeiculoService.oneModelo($stateParams.idModelo);
			},
		}
	})

	.state({
		url: '/veiculo/consultaMarca',
		name: 'veiculo/consultaMarca',
		templateUrl : '/views/veiculo/consultaMarca.html',
		controller : 'consultaMarcaController',
		resolve : {
			"responseCtrl": function(VeiculoService){
				return VeiculoService.todasMarcas();
			},
		}
	})


	.state({
		url: '/veiculo/consultaModelo',
		name: 'veiculo/consultaModelo',
		templateUrl : '/views/veiculo/consultaModelo.html',
		controller : 'consultaModeloController',
		resolve : {
			responseCtrl: function(VeiculoService){
				return VeiculoService.todosModelos();
			}
		}
	})



	.state('veiculo/cadastroCor', {
		url : '/veiculo/cadastroCor',
		templateUrl : '/views/veiculo/cadastroCor.html',
		controller : 'CadastroCorController',
		resolve : {
			"cor" : function() {
				return {}
			}

		}
	})
	

.state('veiculo/cadastroVeiculoMarca',{
	url : '/veiculo/cadastroVeiculoMarca',
	templateUrl : '/views/veiculo/cadastroVeiculoMarca.html',
	controller : 'CadastroMarcaController',
	resolve : {
		"marca" : function() {
		return {};
		}
	},
})

.state('servico/cadastro/:idServico', {
		url: '/servico/cadastro/:idServico',
		templateUrl : '/views/servico/cadastro.html',
		controller : 'CadastroServicoController',
		resolve : {
			"servico" : function(ServicoService,$stateParams) {
				return ServicoService.oneServico($stateParams.idServico);
			},
			"listarAllTipos": function(TipoServicoService){
				return TipoServicoService.listarAllTipos();
			},
			"listarAllFornecedor": function(FornecedorService){
				return FornecedorService.listarAllFornecedor();
			}
		}
	})
	.state({
		url: '/servico/cadastro',
		name: 'servico/cadastro',
		templateUrl : '/views/servico/cadastro.html',
		controller : 'CadastroServicoController',
		resolve : {
			"servico" : function() {
				return {};
			},
			"listarAllTipos": function(TipoServicoService){
				return TipoServicoService.listarAllTipos();
			},
			"listarAllFornecedor": function(FornecedorService){
				return FornecedorService.listarAllFornecedor();
			}

		}
	})

		.state({
		url: '/servico/consulta',
		name: 'servico/consulta',
		templateUrl : '/views/servico/consulta.html',
		controller : 'servicoController',
		resolve : {
			listaServico: function(ServicoService){
				return ServicoService.listarAllServicos();
			}
 


		}
	})


		.state('segmento/cadastro/:idSegmento', {
		url: '/segmento/cadastro/:idSegmento',
		templateUrl : '/views/segmento/cadastro.html',
		controller : 'SegmentoController',
		resolve : {
			segmento : function(SegmentoService,$stateParams) {
				return SegmentoService.oneSegmento($stateParams.idSegmento);
			}
		}
	})



	.state({
		url :'/segmento/cadastro',  
		name: 'segmento/cadastro',
		templateUrl : '/views/segmento/cadastro.html',
		controller : 'SegmentoController',
		resolve : {
			"segmento" : function() {
				return {}
			}

		}
	})

	/////// FORNECEDOR

	.state({
		url :'/fornecedor/cadastro',  
		name: 'fornecedor/cadastro',
		templateUrl : '/views/fornecedor/cadastro.html',
		controller : 'FornecedorController',
		resolve : {
			"responseCtrl" : function() {
				return {};
			},
			"fornecedor": function(){
				return {};
			}

	}
})
		.state({
		url: '/fornecedor/consulta',
		name: 'fornecedor/consulta',
		templateUrl : '/views/fornecedor/consulta.html',
		controller : 'ConsultaFornecedorController',
		resolve : {
			listaFornecedor: function(FornecedorService){
				return FornecedorService.listarAllFornecedor();
			}
		}
	})

		.state('fornecedor/cadastro/:idFornecedor', {
		url: '/fornecedor/cadastro/:idFornecedor',
		templateUrl : '/views/fornecedor/cadastro.html',
		controller : 'FornecedorController',
		resolve : {
			"responseCtrl" : function() {
				return {};
			},
			fornecedor : function(FornecedorService,$stateParams) {
				return FornecedorService.oneFornecedor($stateParams.idFornecedor);
			}
		}
	})

		///UNIDADE DE MEDIDA

		.state({
		url: '/tipoServico/consulta',
		name: 'tipoServico/consulta',
		templateUrl : '/views/tipoServico/consulta.html',
		controller : 'tipoServicoConsulta',
		resolve : {
			"listarAllTipos": function(TipoServicoService){
				return TipoServicoService.listarAllTipos();
			}
		}
	})


		.state({
		url: '/segmento/consulta',
		name: 'segmento/consulta',
		templateUrl : '/views/segmento/consulta.html',
		controller : 'ConsultaSegmentoController',
		resolve : {
			listSegmentos: function(SegmentoService){
				return SegmentoService.listarAllSegmentos();
			
			}
		}
	})

		.state({
		url: '/tipoServico/cadastro/:idTipo',
		name: 'tipoServico/cadastro/unico',
		templateUrl: '/views/tipoServico/cadastro.html',
		controller: 'tipoServicoController',
		resolve: {
				"listOne" : function(TipoServicoService, $stateParams) {
				return TipoServicoService.listOneTipo($stateParams.idTipo);
			},
		}
	})


	.state({
		url: '/tipoServico/cadastro',
		name: 'tipoServico/cadastro',
		templateUrl : '/views/tipoServico/cadastro.html',
		controller : 'tipoServicoController',
		resolve : {
			listOne : function(TipoServicoService) {
				return {};
			}



		}
	});

	 //$urlRouterProvider.when('', '/dashboard');
 	$urlRouterProvider.otherwise('dashboard');
	$httpProvider.interceptors.push('httpInterceptor');
});