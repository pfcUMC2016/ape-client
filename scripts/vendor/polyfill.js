Array.prototype.searchByObject = function(key, value) {
	var arr = this[0];
	var index = -1;
	for (var i = 0, max = arr.length; i < max; i++) {
		var obj = arr[i];
		if (obj[key] == value) index = i;
	}
	
	return index;
}
	
String.prototype.excludeMask = function(string){
	return string.replace(/[^a-zA-Z0-9]/g,'_');
}