module.exports = function(grunt) {
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-watch');
	// Project configuration.
	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
        
		concat: {
			modules: {
				files: {
					"scripts/app.controllers.js": [
                        "scripts/controllers/**/*.js"
					]
					// "scripts/app.services.js": [
					// 	"scripts/services/_initializer.js",						
					// 	"scripts/services/*.js"
					// ]
				} 
			}
		},
		watch: {
                        scripts: {
                                files: ['scripts/controllers/**/*.js'],
                                tasks: ['default']
                        }
                }

	});

	// Default task(s).
	grunt.registerTask('default', ['concat', 'watch']);
};

